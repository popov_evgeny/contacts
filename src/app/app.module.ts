import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ContactsService } from './shared/contacts.service';
import { EditComponent } from './edit/edit.component';
import { ModalComponent } from './ui/modal/modal.component';
import { NotFoundComponent } from './not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    FooterComponent,  // all connected components
    HomeComponent,
    EditComponent,
    ModalComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, // all connected modules
    HttpClientModule,
    FormsModule
  ],
  providers: [ContactsService], // connected service
  bootstrap: [AppComponent]
})
export class AppModule { }
