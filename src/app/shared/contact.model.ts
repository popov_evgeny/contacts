export class Contact {
  constructor(
    public name: string,
    public username: string,
    public email: string,
    public address: Address,    // contact model
    public phone: string,
    public website: string,
    public company: {},
    public posts: [{}],
    public accountHistory: [{}],
    public favorite: boolean,
    public avatar: string,
    public id: number
  ) {}
}


export class Address {
  constructor(
    public streetA: string,
    public streetB: string,
    public streetC: string,  // address model for contact
    public streetD: string,
    public city: string,
    public state: string,
    public country: string,
    public zipcode: string,
    public geo: {}
  ) {}
}
