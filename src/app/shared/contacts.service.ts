import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Contact } from './contact.model';

@Injectable()

export class ContactsService {
  private contactsArray: Contact[] = [];
  changeContactsArray = new Subject<Contact[]>(); // some need variables
  hideSearch = new Subject<boolean>();
  loading = new Subject<boolean>();
  stopRequestsToTheServer = true;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {}


  fetchAllContacts() {                   // server request
    this.loading.next(true);
    this.http.get<{[id: string]: Contact}>('https://demo.sibers.com/users').pipe(map( result => {
      if (result === null) {
        return []
      }
      return Object.keys( result ).map( id => {
        const contact = result[id];
        return new Contact (
          contact.name, contact.username,
          contact.email, contact.address,
          contact.phone, contact.website,
          contact.company, contact.posts,
          contact.accountHistory, contact.favorite,
          contact.avatar, contact.id);
      })
    })).subscribe( result => {
      this.contactsArray = result.sort((a, b) => {
        if(a.name < b.name) { return -1; }
        if(a.name > b.name) { return 1; }
        return 0;
      });
        this.changeContactsArray.next(this.contactsArray.slice());
        this.loading.next(false);
        this.setLocalStorageContacts(this.contactsArray);
    }, () => {
      console.log('yes');
      this.loading.next(false);
    });
  }

  setLocalStorageContacts(array: Contact[]) {  // save contacts array to localStorage
    localStorage.clear();
    localStorage.setItem('contacts', JSON.stringify(array));
  }

  getLocalStorageContactArray() {
    let localStorageContactsArrayData = localStorage.getItem('contacts');   // get contacts array from localStorage
    let localStorageContactsArray = [];
    if (localStorageContactsArrayData) {
      localStorageContactsArray = JSON.parse(localStorageContactsArrayData);
      this.contactsArray = localStorageContactsArray;
      this.changeContactsArray.next(this.contactsArray.slice());
    }
    return localStorageContactsArray;
  }

  getContact(name: string) {
    let contactParams!: Contact;
    this.getLocalStorageContactArray().forEach( (contact: Contact)=> { // find need contact
      if (contact.name === name) {
        contactParams = contact;
      }
    });
    return contactParams;
  }

  setFormDataForContact(id: number, name:string, email:string, website:string, username:string, phone:string) { // method to set new data to contact
    this.contactsArray.forEach( (contact: Contact)=> {
      if (contact.id === id) {
        contact.email = email;
        contact.name = name;
        contact.username = username;
        contact.phone = phone;
        contact.website = website;
      }
    });
    this.setLocalStorageContacts(this.contactsArray.slice());
    this.onHideSearch(false);
    void this.router.navigate(['/']);
  }

  searchContact(searchName: string) {                  // method to search contact
    let searchArr: Contact[] = [];
    this.contactsArray.forEach( (contact: Contact)=> {
      if (contact.name === searchName) {
        searchArr.push(contact);
        this.changeContactsArray.next(searchArr);
      } else if (contact.name.substr(0, searchName.length) === searchName){
        searchArr.push(contact);
        this.changeContactsArray.next(searchArr);
      } else if ( searchName === ''){
        this.changeContactsArray.next(this.contactsArray.slice());
      }
    });
    this.setLocalStorageContacts(this.contactsArray.slice());
  }

  onHideSearch(hide: boolean) { // hide search method
    this.hideSearch.next(hide);
  }
}
