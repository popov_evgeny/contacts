import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ContactsService } from '../shared/contacts.service';
import { Contact } from '../shared/contact.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  contactsArray!: Contact[];
  contact!: Contact;
  changeContactArraySubscription!: Subscription; // some need variables
  loadingSubscription!: Subscription;
  loading!: boolean;
  stopRequests = true;
  isOpenModal = false;

  constructor(
    private contactsService: ContactsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.stopRequests = this.contactsService.stopRequestsToTheServer; // get data request was made to the server
    this.loadingSubscription = this.contactsService.loading.subscribe( loading => {   // get spinner data
      this.loading = loading;
    });
    this.changeContactArraySubscription = this.contactsService.changeContactsArray.subscribe( fetchContactsArray => { // get contacts array data
      this.contactsArray = fetchContactsArray;
    });
    if (this.stopRequests) {
      this.contactsService.fetchAllContacts(); // doing a check to stop the server
    } else {
      this.contactsService.getLocalStorageContactArray();
    }
  }

  onClickContact(id: number, name: string) {
    this.contactsService.stopRequestsToTheServer = false; // subscribe to a button to open a modal window
    this.contactsService.onHideSearch(true);
    void this.router.navigate([id,name,'edit-contact']);
  }

  openModal(id: number) {
    this.isOpenModal = true;
    this.contactsArray.forEach((contact:Contact) => { // modal window opening method
      if (contact.id === id){
        this.contact = contact;
      }
    })
  }

  isClose() {
    this.isOpenModal = false; // modal window closing method
  }

  ngOnDestroy() {
    this.changeContactArraySubscription.unsubscribe(); // unsubscribing from event
  }
}
