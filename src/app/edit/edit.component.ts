import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Contact } from '../shared/contact.model';
import { ContactsService } from '../shared/contacts.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @ViewChild('form') contactForm!: NgForm;  // some need variables
  contact!: Contact;

  constructor(
    private route: ActivatedRoute,
    private contactsService: ContactsService,
  ) {}

  ngOnInit(): void {
    let paramsData = this.route.snapshot.params;
    this.contact = this.contactsService.getContact(paramsData.name); // checking page transition

    if (this.contact) {
      this.setFormValue({
        name: this.contact.name,
        website: this.contact.website,
        username: this.contact.username, // fielding form value
        email: this.contact.email,
        phone: this.contact.phone,
      });
    }
  }

 setFormValue(value: { [key: string]: any }) { // method form fielding form value
    setTimeout(() => {
      this.contactForm.form.setValue(value);
    });
  }

  onSaveChangedContact() { // method saved changes contact
    this.contactsService.setFormDataForContact(this.contact.id, this.contactForm.value.name, this.contactForm.value.email, this.contactForm.value.website, this.contactForm.value.username, this.contactForm.value.phone);
  }
}
