import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ContactsService } from '../../shared/contacts.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit, OnDestroy{  // some need variables
  @ViewChild('search') search!: ElementRef;
  hideSubscription!: Subscription;
  searchName: string = '';
  isHide = false;

  constructor(
    private contactService: ContactsService,
  ) {}

  ngOnInit() {
    this.hideSubscription = this.contactService.hideSearch.subscribe( hide => { // get data for search
      this.isHide = hide;
    });
  }

  searchContact() {
    this.searchName = this.capitalize(this.search.nativeElement.value);  // method to find contact
    this.contactService.searchContact(this.searchName);
  }

  capitalize(word: string) {
    if (word !== undefined && word !== '') {
      return word[0].toUpperCase() + word.slice(1).toLowerCase(); // method to find contact
    } else { return '' }
  }

  returnHome() {
    this.isHide = false;  // method to display search
  }

  ngOnDestroy() {
    this.hideSubscription.unsubscribe(); // unsubscribing from event
  }
}
